Task 1 : ​Create a sample maven project from command prompt with quick start archetype​.

1. Choose a desired path for your project
2. then type "mvn archetype:generate"
3. use code 2131 (it is for quickstart)
4. select maven quickstart version (latest is 1.4)
5. Then it will ask you groupId followed by artifactId, version and package
6. confirm it by y, thats it.

Task 2 : Also use Maven life cycle to execute and verify the files/folders generated.​

Validate - This phase validates the project for instance, if all the neccessary information is available to build the project.
	it creates target dir which consists of sub dirs like 'generated-source' and 'maven-status'

Compile - In this phase the project source code is compiled.
 	it creates sub dir in target i.e 'classes'

Package - The compiled code and the resources are packaged into a distributable format. Such as JAR, WAR.

Clean - it removes the target folder

Verify - it generate resources followed by compiling the module then generate test resources followed by test compile and generates deployable package (JAR/WAR)

Install - generate,compile, test, integration-test, package, install in local repo then generate target folder

Site - It generates reports regarding components such as ci-management, dependencies, dependency-info, dependency-management, distribution-management, index, issue-management, licenses, mailing-lists, modules, plugin-management, plugins, scm, summary, team

After performing Task 1 & Task 2, create a word document on Maven life-cycle and its phases. Add the observations you made along with why you have this observation.


Optional Task 3 : Explore other build tool Gradle.

